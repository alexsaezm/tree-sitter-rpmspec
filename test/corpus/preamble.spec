=================
Preamble Section
=================

Name:           tree-sitter-rpmspec
Version:        1.0
Release:        %autorelease
Summary:        A tree-sitter parser for RPM Spec

License:        MIT
URL:            https://gitlab.com/cryptomilk/tree-sitter-rpmspec/
Source0:        https://gitlab.com/cryptomilk/tree-sitter-rpmspec/-/archive/main/tree-sitter-rpmspec-main.tar.gz

BuildRequires:  tree-sitter-cli
Requires:       rpm
