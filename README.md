tree-sitter-rpmspec
===================

A tree-sitter parser for RPM spec files.

## Generating the parser

```sh
tree-sitter generate
```

Learn more about writing a parser [here](https://tree-sitter.github.io/tree-sitter/creating-parsers).
